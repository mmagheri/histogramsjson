#pragma once

#include <filesystem>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <time.h>
#include "histogram.hpp"


class histogram1D : public histogram {
private:
    //std::string name;
    std::vector<float> bin_limits;
    std::vector<unsigned  long  long> bin_content; 
public:

    histogram1D(std::string name_, int nBins, float min, float max) : histogram(name_) {
        this->SetName(name_);
        float step = (max-min)/nBins;
        float pos = min;
        while(pos < max) {
            bin_limits.push_back(pos);
            pos += step;
        }
        bin_content = std::vector<unsigned  long  long>(bin_limits.size(), 0);
    };
    
    histogram1D(std::string name_, std::vector<float> xs) : histogram(name_) {
        this->SetName(name_);
        bin_limits = xs;
        bin_content = std::vector<unsigned  long  long>(bin_limits.size(), 0);
    };

    histogram1D( std::string name_, std::vector<float> xs, std::vector<unsigned  long  long> binCont) : histogram(name_) {
        this->SetName(name_);
        if(xs.size()!=binCont.size()) {
            std::cout << "ERRROR: could not initialize histogram, different length of bin limits and bin content\n";
        }
        bin_limits = xs;
        bin_content = binCont;
    };
    
    ~histogram1D();
    bool Fill(int value);
    void Print();
    void Reset();
    bool Save(std::string fname, bool app, bool simple);
    bool SaveSingleFile(std::ofstream &o);
    std::vector<unsigned  long  long> GetBinContent() { return bin_content; };
    std::vector<float> GetBinLimits() { return bin_limits; };
    histogram1D Add(histogram1D h);

};