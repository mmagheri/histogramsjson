#pragma once
#include <fstream>

#include "histogram.hpp"

class histogram2D : public histogram {
private:
    std::vector<std::vector<float>> bin_limits;
    std::vector<std::vector<unsigned  long  long>> bin_content; 
    
public:

    histogram2D(std::string name_, int nBinsX, float minX, float maxX, int nBinsY, float minY, float maxY) : histogram(name_) {
        this->SetName(name_);
        float stepX = (maxX-minX)/nBinsX;
        float stepY = (maxY-minY)/nBinsY;
        float posX = minX;
        float posY = minY;
        std::vector<float> xAxis;
        std::vector<float> yAxis;

        while(posX < maxX) {
            xAxis.push_back(posX);
            posX += stepX;
        }
        while(posY < maxY) {
            yAxis.push_back(posY);
            posY += stepY;
        }
        bin_limits.push_back(xAxis);
        bin_limits.push_back(yAxis);
        for(int i=0; i<bin_limits[0].size(); i++) bin_content.push_back(std::vector<unsigned  long  long>(bin_limits[1].size(), 0));
    };
    
    histogram2D(std::string name_, std::vector<float> xs, std::vector<float> ys) : histogram(name_) {
        this->SetName(name_);
        bin_limits.push_back(xs);
        bin_limits.push_back(ys);
        for(int i=0; i<bin_limits[0].size(); i++) bin_content.push_back(std::vector<unsigned  long  long>(bin_limits[1].size(), 0));
    };
    
    ~histogram2D();
    bool Fill(float x, float y);
    void Reset();
    void Print();
    bool Save(std::string fname, bool app, bool simple);
    bool SaveSingleFile(std::ofstream &o);
};