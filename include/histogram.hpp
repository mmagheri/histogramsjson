#pragma once

#include <filesystem>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <time.h> 
#include <map>

class histogram
{
private:
    std::string name;
    std::map<std::string, std::string> axis; 
    std::string plotOptions;
    bool isDQM;
public:
    histogram(std::string name_);
    std::string SetName(std::string name_);
    std::string GetName();
    std::string SetAxis(std::string axis_, std::string name_);
    std::string GetAxis(std::string axis_);
    std::string SetPlotOptions (std::string plotOption_);
    std::string GetPlotOptions ();
    bool SetDQM (bool isDQM_);
    bool GetDQM ();
};