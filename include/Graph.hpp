#pragma once

#include <filesystem>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <time.h>
#include "histogram.hpp"


class graph : public histogram {
private:
    //std::string name;
    std::vector<float> x;
    std::vector<float> y;
    float aggregateTime;
public:

    graph(std::string name_, float aggregateTime_ = 0) : histogram(name_) {this->SetName(name_); aggregateTime = aggregateTime_;};

    graph(std::string name_, std::vector<float> x_, std::vector<float> y_, float aggregateTime_ = 0) : histogram(name_) {
        this->SetName(name_);
        aggregateTime = aggregateTime_;
        x = x_;
        y = y_;
    };
    
    ~graph();
    bool Fill(float x_, float y_);
    bool Save(std::string fname, bool app, bool simple);
    bool SaveSingleFile(std::ofstream &o);
    void Reset() {x.clear(); y.clear();};
    std::vector<float> GetX() {return x;};
    std::vector<float> GetY() {return y;};
    graph Add(graph h);
    void Print();
};