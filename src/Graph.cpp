#include "Graph.hpp"


graph::~graph() {
    x.clear();
    y.clear();
}

bool graph::Fill(float x_, float y_) {
    if (x.size() == 0) {
        x.push_back(x_);
        y.push_back(y_);
        return true;
    }
    else if(abs(x_ - x.back()) < aggregateTime) {
        y.back() += + y_;
        return false;
    }
    else if(abs(x_ - x.back()) > aggregateTime) {
        x.push_back(x_);
        y.push_back(y_);
        return true;
    }
    return false;
}


void graph::Print() {
    std::cout << "--- Printing graph ---\n";
    std::cout << "X:: \n";
    for(auto &l : x) std::cout << std::setw(10) << l << "\t";
    std::cout << "\ny: \n";
    for(auto &l : y) std::cout << std::setw(10) << l << "\t";
    std::cout << "\n";
}

bool graph::Save(std::string fname, bool app, bool simple) {
    std::ofstream o; //ofstream is the class for fstream package
    if (!app) o.open(fname + "_" + this->GetName() + ".json"); //open is the method of ofstream
    if (app) {
        o.open(fname + this->GetName() + ".json", std::ofstream::app);
        o << "\n";
    }
    if(!simple){
        o << "{\n";
        o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
        o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
        o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
        o << "\t \"dim\": 1,\n"; 
        o << "\t\"name\": \""<< this->GetName() << "\",\n";
        o << "\t \"x\": [\n";
        for(int i=0 ; i<x.size(); i++) {
            o << "\t\t\t" << x[i];
            if(i!=x.size()-1) o << ",\n";
        }
        o << "],";
        o << "\n \t \"y\": [\n";
        for(int i=0 ; i<y.size(); i++) {
            o << "\t\t\t" << y[i];
            if(i!=y.size()-1) o << ",\n";
        }
        o << "\n\t\t]";
        o << "\n}";
        o.close();
        return true;
    }
    if(simple) { 
        o << "{\n";
        o << "\t\"name\": \""<< this->GetName() << "\",\n";
        o << "\t\"x\": [ \n";
        for(int i=0 ; i<x.size(); i++) {
            o << "\t\t" << x[i];
            if(i!=x.size()-1) o << ",\n";
        }
        o << "],\n";
        o << "\t\"y\": [\n";
        for(int i=0 ; i<y.size(); i++) {
            o << "\t\t" << y[i];
            if(i!=y.size()-1) o << ",\n";
        }
        o << "\n\t]";
        o << "}";
        o.close();
        return true;
    }
    return false;
}


bool graph::SaveSingleFile(std::ofstream &o) {
    o << "\""<< this->GetName() << "\":\n";
    o << "{\n";
    o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
    o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
    o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
    o << "\t \"dim\": 1,\n"; 
    o << "\t\"name\": \""<< this->GetName() << "\",\n";
    o << "\t \"x\": [\n";
    for(int i=0 ; i<x.size(); i++) {
        o << "\t\t\t" << x[i];
        if(i!=x.size()-1) o << ",\n";
    }
    o << "],";
    o << "\n \t \"y\": [\n";
    for(int i=0 ; i<y.size(); i++) {
        o << "\t\t\t" << y[i];
        if(i!=y.size()-1) o << ",\n";
    }
    o << "\n\t\t]";
    o << "\n}";
    return true;

}

graph graph::Add(graph h) {
    graph g2(this->GetName(), this->GetX(), this->GetY());
    std::vector<float> xToAdd = h.GetX();
    std::vector<float> yToAdd = h.GetY();
    for(int i = 0; i < xToAdd.size(); i++) g2.Fill(xToAdd[i], yToAdd[i]);
    return g2;
}

