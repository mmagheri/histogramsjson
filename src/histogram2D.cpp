#include "histogram2D.hpp"


histogram2D::~histogram2D() {
    bin_content.clear();
    bin_limits.clear();
}

bool histogram2D::Fill(float x, float y) {
    int binX = int ( bin_limits[0].size() * (x-bin_limits[0][0])/(1.0*bin_limits[0][bin_limits[0].size()-1]-1.0*bin_limits[0][0]));
    int binY = int ( bin_limits[1].size() * (y-bin_limits[1][0])/(1.0*bin_limits[1][bin_limits[1].size()-1]-1.0*bin_limits[1][0]));
    if (binX>bin_content[0].size()) {
        std::cout << "bin content too high in " << this->GetName() << "\n";
        binX = bin_content[0].size() -1;
    }
    if (binX>bin_content[1].size()) {
        std::cout << "bin content too low in " << this->GetName() << "\n";
        binX = bin_content[1].size() -1;
    }
    bin_content[binX][binY]++;
    return true;
}

void histogram2D::Print() {
    std::cout << "--- Printing histogram ---\n";
    std::cout << "Histogram limits: \n";
    for (int i = 0; i < bin_limits[0].size(); i++) {
        for (int j=0; j<bin_limits[1].size(); j++) {
            std::cout << std::setw(10) << bin_content[i][j] << "  ";
        }
        std::cout << "\n";
    }
    std::cout << "\nBin content: \n";
    for (int i = 0; i < bin_content[0].size(); i++) {
        for (int j=0; j<bin_content[1].size(); j++) {
            std::cout << std::setw(10) << bin_content[i][j] << "  ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

void histogram2D::Reset() {
    for(auto & bin: bin_content) for(auto& con : bin) con = 0;
}

bool histogram2D::Save(std::string fname, bool app, bool simple) {

    std::ofstream o; //ofstream is the class for fstream package

    if (!app) o.open(fname + "_" + this->GetName() + ".json"); //open is the method of ofstream
    if (app) {
        o.open(fname + "_" + this->GetName() + ".json", std::ofstream::app);
        o << "\n";
    }
    if(simple){
        return false;
    }
    if(!simple) { 
        o << "{\n";
        o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
        o << "\t \"yLabel\": \"" << this->GetAxis("y") << "\",\n";
        o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
        o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
        o << "\t \"dim\": 2,\n";
        o << "\t\"name\": \""<< this->GetName() << "\",\n";
        o << "\t\"x\": [ \n";
        for(int i=0 ; i<bin_limits[0].size(); i++) {
            o << "\t" << bin_limits[0][i];
            if(i!=bin_limits[0].size()-1) o << ",\n";
        }
        o<< "\n\t],\n";
        o << "\t\"y\": [ \n";
        for(int i=0 ; i<bin_limits[1].size(); i++) {
            o  << "\t" << bin_limits[1][i];
            if(i!=bin_limits[0].size()-1) o << ",\n";
        }
        o << "\n\t],\n";
        o << "\t\"data\": [ \n";
        for(int i=0 ; i<bin_content.size(); i++) {
            auto bc = bin_content[i];
            o << "[";
            for(int j=0; j<bc.size(); j++) {
                o << bc[j];
                if(j!=bc.size()-1) o << ",";
            }
            o << "]";
            if(i!=bin_content.size()-1) o << ",";
            o<<"\n";
        }

        o << "\n\t]";
        o << "}";
        o.close();
        return true;
    }
    return false;
}


bool histogram2D::SaveSingleFile(std::ofstream &o) {
    o << "\""<< this->GetName() << "\":\n";
    o << "{\n";
    o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
    o << "\t \"yLabel\": \"" << this->GetAxis("y") << "\",\n";
    o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
    o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
    o << "\t \"dim\": 2,\n";
    o << "\t\"name\": \""<< this->GetName() << "\",\n";
    o << "\t\"x\": [ \n";
    for(int i=0 ; i<bin_limits[0].size(); i++) {
        o << "\t" << bin_limits[0][i];
        if(i!=bin_limits[0].size()-1) o << ",\n";
    }
    o<< "\n\t],\n";
    o << "\t\"y\": [ \n";
    for(int i=0 ; i<bin_limits[1].size(); i++) {
        o  << "\t" << bin_limits[1][i];
        if(i!=bin_limits[0].size()-1) o << ",\n";
    }
    o << "\n\t],\n";
    o << "\t\"data\": [ \n";
    for(int i=0 ; i<bin_content.size(); i++) {
        auto bc = bin_content[i];
        o << "[";
        for(int j=0; j<bc.size(); j++) {
            o << bc[j];
            if(j!=bc.size()-1) o << ",";
        }
        o << "]";
        if(i!=bin_content.size()-1) o << ",";
        o<<"\n";
    }

    o << "\n\t]";
    o << "}";
    return true;
}
