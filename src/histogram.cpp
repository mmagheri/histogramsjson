#include "histogram.hpp"


histogram::histogram(std::string name_) {
    this->SetName(name_);
    axis.insert(std::pair<std::string, std::string>("x", ""));
    axis.insert(std::pair<std::string, std::string>("y", ""));
    plotOptions = "";
    isDQM = true;
}

std::string histogram::SetName(std::string name_) {
    name = name_;
    return name;
};

std::string histogram::GetName() {
    return name;
};

std::string histogram::SetAxis(std::string axis_, std::string name_) {

    if (axis.find(axis_) == axis.end()) {
        std::cout << "ERROR in setting the axis, you can set only x or y axis, you are trying to set: " << axis_ <<"\n";
        return "";
    } else {
        axis[axis_] = name_;
        return axis[axis_];
    }
}

std::string histogram::GetAxis(std::string axis_) {
    if (axis.find(axis_) == axis.end()) {
        std::cout << "ERROR in getting the axis, you can get only x or y axis, you are trying to get: " << axis_ <<"\n";
        return "";
    } else {
        return axis[axis_];
    }
}

std::string histogram::SetPlotOptions(std::string plotOption_) {
    plotOptions = plotOption_;
    return plotOptions;
}

std::string histogram::GetPlotOptions() { return plotOptions; }

bool histogram::SetDQM(bool isDQM_) { isDQM = isDQM_; return isDQM;}

bool histogram::GetDQM() { return isDQM; }