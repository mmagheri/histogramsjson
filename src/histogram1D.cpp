#include "histogram1D.hpp"


histogram1D::~histogram1D() {
    bin_limits.clear();
    bin_content.clear();
}

bool histogram1D::Fill(int value) {
    //std::cout << 1 << " " ;
    //std::cout << bin_limits.size();
    //std::cout << " " << value;
    //std::cout << " " << bin_limits[bin_limits.size()-1];
    //std::cout << " "<< bin_limits[0] << std::endl;
    //std::cout << 1.0*bin_limits[-1]-1.0*bin_limits[0] << std::endl;
    int bin = int ( bin_limits.size() * (value-bin_limits[0])/(1.0*bin_limits[bin_limits.size()-1]-1.0*bin_limits[0]));
    //std::cout << 1.0*bin_limits[bin_limits.size()]<< std::endl;
    //std::cout << bin << " " << bin_content.size() << std::endl;
    if(bin > bin_content.size()-1) {
        
        std::cout << "Bin content ("<< bin<< ", " << value << ") too high in " << this->GetName() << std::endl;
        bin_content[bin_content.size()-1]++;
    }
    else if(bin<0) {
        std::cout << "bin content too low in" << this->GetName() << "\n";
        bin_content[0]++;
    }
    else bin_content[bin]++;
    //for(auto i = 0; i < size-1; i++) {
    //    if(value < bin_limits[i+1] && value >= bin_limits[i]) {
    //        bin_content[i]++;
    //        return true;
    //    }
    //}
    //bin_content[size-1]++;
    return true;
}

void histogram1D::Reset() {
    for(auto & con: bin_content) con = 0;
}

void histogram1D::Print() {
    std::cout << "--- Printing histogram ---\n";
    std::cout << "Histogram limits: \n";
    for(auto &l : bin_limits) std::cout << std::setw(10) << l << "\t";
    std::cout << "\nBin content: \n";
    for(auto &l : bin_content) std::cout << std::setw(10) << l << "\t";
    std::cout << "\n";
}

bool histogram1D::Save(std::string fname, bool app, bool simple) {
    std::ofstream o; //ofstream is the class for fstream package
    if (!app) o.open(fname + "_" + this->GetName() + ".json"); //open is the method of ofstream
    if (app) {
        o.open(fname + this->GetName() + ".json", std::ofstream::app);
        o << "\n";
    }
    if(!simple){
        o << "{\n";
        o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
        o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
        o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
        o << "\t \"dim\": 1,\n"; 
        o << "\t\"name\": \""<< this->GetName() << "\",\n";
        o << "\t \"x\": [\n";
        for(int i=0 ; i<bin_limits.size(); i++) {
            o << "\t\t\t" << bin_limits[i];
            if(i!=bin_limits.size()-1) o << ",\n";
        }
        o << "],";
        o << "\n \t \"y\": [\n";
        for(int i=0 ; i<bin_content.size(); i++) {
            o << "\t\t\t" << bin_content[i];
            if(i!=bin_limits.size()-1) o << ",\n";
        }
        o << "\n\t\t]";
        o << "\n}";
        o.close();
        return true;
    }
    if(simple) { 
        o << "{\n";
        o << "\t\"name\": \""<< this->GetName() << "\",\n";
        o << "\t\"x\": [ \n";
        for(int i=0 ; i<bin_limits.size(); i++) {
            o << "\t\t" << bin_limits[i];
            if(i!=bin_limits.size()-1) o << ",\n";
        }
        o << "],\n";
        o << "\t\"y\": [\n";
        for(int i=0 ; i<bin_content.size(); i++) {
            o << "\t\t" << bin_content[i];
            if(i!=bin_limits.size()-1) o << ",\n";
        }
        o << "\n\t]";
        o << "}";
        o.close();
        return true;
    }
    return false;
}


bool histogram1D::SaveSingleFile(std::ofstream &o) {
    o << "\""<< this->GetName() << "\":\n";
    o << "{\n";
    o << "\t \"xLabel\": \"" << this->GetAxis("x") << "\",\n";
    o << "\t \"plotOptions\": \"" << this->GetPlotOptions() << "\",\n";
    o <<"\t \"isDQM\":" << this->GetDQM() << ",\n"; 
    o << "\t \"dim\": 1,\n"; 
    o << "\t\"name\": \""<< this->GetName() << "\",\n";
    o << "\t \"x\": [\n";
    for(int i=0 ; i<bin_limits.size(); i++) {
        o << "\t\t\t" << bin_limits[i];
        if(i!=bin_limits.size()-1) o << ",\n";
    }
    o << "],";
    o << "\n \t \"y\": [\n";
    for(int i=0 ; i<bin_content.size(); i++) {
        o << "\t\t\t" << bin_content[i];
        if(i!=bin_limits.size()-1) o << ",\n";
    }
    o << "\n\t\t]";
    o << "\n}";
    return true;

}


histogram1D histogram1D::Add(histogram1D h) {
    if(h.GetBinLimits() != bin_limits) {
        std::cout << "Impossible to add the two histograms, returning the called one\n";
        return *this;
    }
    std::vector<unsigned long long> otherHistoContent = h.GetBinContent();
    std::vector<unsigned long long> addedBin;
    for (int i = 0; i < bin_content.size(); i++) addedBin.push_back(otherHistoContent[i] + bin_content[i]);
    return histogram1D( this->GetName(), bin_limits, addedBin);
}

